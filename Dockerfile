FROM python:3.8-slim-buster
MAINTAINER arsene
WORKDIR /code
COPY requirements.txt .
RUN pip install -r requirements.txt
COPY appli/ .
CMD [ "python", "./lancement.py" ]


