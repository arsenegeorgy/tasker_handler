from app import app
from crud import *
from flask import jsonify, request


@app.route('/', methods=['GET', 'POST'])
def all_tasks():
    tsks = get_all_task()
    cols = ['id', 'title', 'contain', 'state']
    data = [{col: getattr(d, col) for col in cols} for d in tsks]
    return jsonify(data=data)


# @app.route("/addtask/<str:title>/<str:contain>", methods=['GET', 'POST'])
@app.route("/addtask", methods=['GET', 'POST'])
def addtask():
    # get data by post
    data = request.get_json()
    tsk = create_new_task(data["title"], data["contain"])
    return jsonify({'id': str(tsk.id)})


@app.route("/deletetask", methods=['GET', 'POST'])
def deletetask():
    # get data by get and delete
    data = request.get_json()
    rm_task(data["id"])
    return jsonify({'status': 'done'})






