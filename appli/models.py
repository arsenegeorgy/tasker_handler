from app import db


class Tasks(db.Model):
    """
    class model to store tasks
    """
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(80), unique=True, nullable=False)
    contain = db.Column(db.String(120), unique=True, nullable=False)
    state = db.Column(db.Integer)
