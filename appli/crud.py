from models import *


def create_new_task(tsk_title, tsk_contain):
    """
    adding a task
    :param tsk_title: the task title
    :param tsk_contain: the task contain
    :return: object
    """
    tsk = Tasks(title=tsk_title, contain=tsk_contain, state=1)
    db.session.add(tsk)
    db.session.commit()
    return tsk


def get_task_by_title(tsk_title):
    """
    get task by title
    :param tsk_title:
    :return:
    """
    task = Tasks.query.filter_by(title=tsk_title).first()
    return task


def get_all_task():
    """
    get all the task in the data base
    :return:
    """
    tasks = Tasks.query.filter_by(state=1).all()
    return tasks


def rm_task(tsk_id):
    task = Tasks.query.filter_by(id=tsk_id).first()
    print(task.state)
    task.state = 0
    # db.session.add(task)
    db.session.commit()
    return task




